import Vue from 'vue'
import Vuex from 'vuex'
import login from './login'

Vue.use(Vuex)

// 創建 store 實例
const store = new Vuex.Store({
  modules: {
    login
  }
})

export default store
