export default {
  state: {
    newRouter: [],
    token: sessionStorage.getItem('token'),
    locale: sessionStorage.getItem('locale')
  },
  mutations: {
    SET_NEW_ROUTER: (state, newRouter) => {
      state.newRouter = newRouter
    },
    SET_TOKEN: (state, token) => {
      state.token = token
    },
    SET_LOCALE: (state, locale) => {
      state.locale = locale
    }
  },
  actions: {
    async submitLogin ({ commit }, data) {
      const token = data.data.token
      const language = sessionStorage.getItem('locale')
      sessionStorage.setItem('token', token)
      sessionStorage.setItem('locale', language)
      commit('SET_NEW_ROUTER', '')
      commit('SET_TOKEN', token)
      commit('SET_LOCALE', '')
      commit('SET_LOCALE', language)
    },
    async Logout ({ commit }) {
      return new Promise((resolve, reject) => {
        const language = sessionStorage.getItem('locale')
        commit('SET_NEW_ROUTER', '')
        commit('SET_TOKEN', '')
        commit('SET_LOCALE', language)
        sessionStorage.clear()
        sessionStorage.setItem('locale', language)
        resolve('logout')
      }).catch(error => {
        this.reject(error)
      })
    },
    addRoutes ({ commit }, newRouter) {
      return new Promise((resolve, reject) => {
        commit('SET_NEW_ROUTER', newRouter)
        resolve(newRouter)
      }).catch(error => {
        this.reject(error)
      })
    }
  }
}
