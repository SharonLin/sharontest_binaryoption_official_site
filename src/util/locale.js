export const localeMap = {
  // 简中
  zh: 'cn',
  cn: 'cn',
  //  繁中
  // 'zh-tw': 'tw',
  // tw: 'tw',
  //  英文
  en: 'en',
  //  泰文
  th: 'th',
  //  越文
  vi: 'vi',
  //  西文
  es: 'es'
  //  俄文
  // ru: 'ru'
}

export const languageSet = () => {
  const originalLanguage = (navigator.language || navigator.browserLanguage).toLowerCase()
  sessionStorage.setItem('originalLanguage', originalLanguage)
  console.log(navigator)
  const language = languageMapping(originalLanguage) ? languageMapping(originalLanguage) : 'en' // 預設中文
  if (!sessionStorage.getItem('locale')) {
    sessionStorage.setItem('locale', language)
  }
}

const languageMapping = originalLanguage => {
  let languageAfterMapping = ''
  languageList.forEach(language => {
    if (originalLanguage.includes(language)) {
      if (language === 'zh' && originalLanguage === 'zh-tw') {
        // 避免繁中轉成簡中
        // 避免繁中轉成簡中
        // 避免繁中轉成簡中
      } else {
        languageAfterMapping = language
      }
    }
  })
  return languageAfterMapping
}

const languageList = ['zh', 'en', 'th', 'vi', 'es']
