export function strip (num, precision = 12) {
  return +parseFloat(num.toPrecision(precision))
}

export function formatNum (num) {
  // 0也要能留下來，所以只擋undefined
  if (num !== undefined) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
  } else {
    return ''
  }
}
