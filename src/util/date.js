export const pickerOptions = {
  shortcuts: [
    {
      text: 'today',
      i18nKey: 'optionToday',
      value () {
        const end = new Date()
        end.setFullYear(end.getFullYear())
        end.setMonth(end.getMonth())
        end.setDate(end.getDate())
        end.setHours(23, 59, 59)
        const start = new Date()
        start.setFullYear(start.getFullYear())
        start.setMonth(start.getMonth())
        start.setDate(start.getDate())
        start.setHours(0, 0, 0)
        return [start, end]
      }
    },
    {
      text: 'yesterday',
      i18nKey: 'optionYesterday',
      value () {
        const end = new Date()
        end.setTime(end.getTime() - 3600 * 1000 * 24)
        end.setFullYear(end.getFullYear())
        end.setMonth(end.getMonth())
        end.setDate(end.getDate())
        end.setHours(23, 59, 59)
        const start = new Date()
        start.setTime(start.getTime() - 3600 * 1000 * 24)
        start.setFullYear(start.getFullYear())
        start.setMonth(start.getMonth())
        start.setDate(start.getDate())
        start.setHours(0, 0, 0)
        return [start, end]
      }
    },
    {
      text: '1 week',
      i18nKey: 'optionWeek1',
      value () {
        const end = new Date()
        const start = new Date()
        start.setTime(start.getTime() - 3600 * 1000 * 24 * 7)
        return [start, end]
      }
    },
    {
      text: '1 month',
      i18nKey: 'optionMonth1',
      value () {
        const end = new Date()
        const start = new Date()
        start.setTime(start.getTime() - 3600 * 1000 * 24 * 30)
        return [start, end]
      }
    },
    {
      text: '3 months',
      i18nKey: 'optionMonth3',
      value () {
        const end = new Date()
        const start = new Date()
        start.setTime(start.getTime() - 3600 * 1000 * 24 * 90)
        return [start, end]
      }
    },
    {
      text: 'thisMonth',
      i18nKey: 'thisMonth',
      value () {
        const end = new Date()
        const date = dateMonthToString(end)
        const year = date.substr(0, 4)
        const month = date.substr(4)
        const isLeap = isLeapYear(year)
        let day = dayOfLastDay(month, isLeap)
        end.setFullYear(end.getFullYear())
        end.setMonth(end.getMonth())
        end.setDate(day)
        end.setHours(23, 59, 59)
        const start = new Date()
        start.setFullYear(start.getFullYear())
        start.setMonth(start.getMonth())
        start.setDate(1)
        start.setHours(0, 0, 0)
        return [start, end]
      }
    },
    {
      text: 'lastMonth',
      i18nKey: 'lastMonth',
      value () {
        const end = new Date()
        let date = end
        const newMonth = end.getMonth() - 1
        date.setMonth(newMonth)
        date = dateMonthToString(date)
        const year = date.substr(0, 4)
        const month = date.substr(4)
        const isLeap = isLeapYear(year)
        let day = dayOfLastDay(month, isLeap)
        end.setFullYear(end.getFullYear())
        end.setMonth(newMonth)
        end.setDate(day)
        end.setHours(23, 59, 59)
        const start = new Date()
        start.setFullYear(start.getFullYear())
        start.setMonth(start.getMonth() - 1)
        start.setDate(1)
        start.setHours(0, 0, 0)
        return [start, end]
      }
    }
  ]
}

export const pickerOptionsJustOneDay = {
  shortcuts: [
    {
      text: 'today',
      i18nKey: 'optionToday',
      value () {
        const start = new Date()
        return start
      }
    },
    {
      text: 'yesterday',
      i18nKey: 'optionYesterday',
      value () {
        const start = new Date()
        start.setTime(start.getTime() - 3600 * 1000 * 24)
        return start
      }
    },
    {
      text: '1 week',
      i18nKey: 'optionWeek1',
      value () {
        const start = new Date()
        start.setTime(start.getTime() - 3600 * 1000 * 24 * 7)
        return start
      }
    },
    {
      text: '1 month',
      i18nKey: 'optionMonth1',
      value () {
        const start = new Date()
        start.setTime(start.getTime() - 3600 * 1000 * 24 * 30)
        return start
      }
    },
    {
      text: '3 months',
      i18nKey: 'optionMonth3',
      value () {
        const start = new Date()
        start.setTime(start.getTime() - 3600 * 1000 * 24 * 90)
        return start
      }
    }
  ]
}

export function stringToDate (date, separator) {
  date = date.toString()
  if (date.length === 1) {
    return ''
  } else {
    const year = date.substr(0, 4)
    const month = date.substr(4, 2)
    const day = date.substr(6, 2)
    const hour = date.substr(8, 2)
    const minute = date.substr(10, 2)
    const second = date.substr(12, 2)
    const result = `${year}${separator}${month}${separator}${day} ${hour}:${minute}:${second}`
    return result
  }
}

export function stringToDateOnly (date, separator) {
  date = date.toString()
  if (date.length === 1) {
    return ''
  } else {
    const year = date.substr(0, 4)
    const month = date.substr(4, 2)
    const day = date.substr(6, 2)
    const result = `${year}${separator}${month}${separator}${day}`
    return result
  }
}

export function stringToMonthOnly (date, separator) {
  date = date.toString()
  if (date.length === 1) {
    return ''
  } else {
    const year = date.substr(0, 4)
    const month = date.substr(4, 2)
    const result = `${year}${separator}${month}`
    return result
  }
}

export function dateToString (date) {
  const year = date.getFullYear()
  let month = date.getMonth() + 1
  month = month < 10 ? `0${month}` : month
  let day = date.getDate()
  day = day < 10 ? `0${day}` : day
  const result = `${year}${month}${day}`
  return result
}
export function dateMonthToString (date) {
  const year = date.getFullYear()
  let month = date.getMonth() + 1
  month = month < 10 ? `0${month}` : month
  const result = `${year}${month}`
  return result
}
export function dateHourToString (date) {
  const year = date.getFullYear()
  let month = date.getMonth() + 1
  month = month < 10 ? `0${month}` : month
  let day = date.getDate()
  day = day < 10 ? `0${day}` : day
  let hour = date.getHours()
  hour = hour < 10 ? `0${hour}` : hour
  const result = `${year}${month}${day}${hour}`
  return result
}
export function dateSecondToString (date) {
  const year = date.getFullYear()
  let month = date.getMonth() + 1
  month = month < 10 ? `0${month}` : month
  let day = date.getDate()
  day = day < 10 ? `0${day}` : day
  let hour = date.getHours()
  hour = hour < 10 ? `0${hour}` : hour
  let minute = date.getMinutes()
  minute = minute < 10 ? `0${minute}` : minute
  let second = date.getSeconds()
  second = second < 10 ? `0${second}` : second
  const result = `${year}${month}${day}${hour}${minute}${second}`
  return result
}

export function dateStringFormat (str) {
  const date = new Date(str.toString())
  const year = date.getUTCFullYear()
  let month = date.getMonth() + 1
  month = month < 10 ? `0${month}` : month
  let day = date.getDate()
  day = day < 10 ? `0${day}` : day
  let hour = date.getHours()
  hour = hour < 10 ? `0${hour}` : hour
  let minute = date.getMinutes()
  minute = minute < 10 ? `0${minute}` : minute
  let second = date.getSeconds()
  second = second < 10 ? `0${second}` : second
  const result = `${year}${month}${day}${hour}${minute}${second}`
  return result
}

function isLeapYear (year) {
  let isLeap = true
  if ((year % 4 === 0 && year % 100 !== 0) || year % 400 === 0) {
    isLeap = true
  } else {
    isLeap = false
  }
  return isLeap
}

function dayOfLastDay (month, isLeap) {
  let day = ''
  if (month.match(/^01|03|05|07|08|10|12/)) {
    day = 31
  }
  if (month.match(/^04|06|09|11/)) {
    day = 30
  }
  if (month.match(/^02/) && isLeap) {
    day = 29
  }
  if (month.match(/^02/) && !isLeap) {
    day = 28
  }
  return day
}

export function lastDayOfThisMonth () {
  let date = new Date()
  date = dateMonthToString(date)
  const year = date.substr(0, 4)
  const month = date.substr(4)
  const isLeap = isLeapYear(year)
  let day = dayOfLastDay(month, isLeap)
  const result = `${year}${month}${day}`
  return result
}

export function lastDayOfLastMonth () {
  let date = new Date()
  const newMonth = date.getMonth() - 1
  date.setMonth(newMonth)
  date = dateMonthToString(date)
  const year = date.substr(0, 4)
  const month = date.substr(4)
  const isLeap = isLeapYear(year)
  let day = dayOfLastDay(month, isLeap)
  const result = `${year}${month}${day}`
  return result
}
