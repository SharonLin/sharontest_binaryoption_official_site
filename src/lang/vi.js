export default {
  devTitle: 'AK FINANCIAL',
  index: '首页',
  info: '消息',
  // menu
  home: 'Trang chủ',
  about: 'Liên quan đến chúng tôi',
  join: 'Gia nhập với chúng tôi',
  // banner
  findOutMore: 'Hiểu thêm nhiều',
  // about us
  aboutText: 'Phương pháp quản lý tài chính có sự tham gia của cộng đồng bao gồm các quỹ, cổ phiếu, đại lý nhượng quyền, v.v. tùy chọn.Là một trong số đó, AK FINANCIAL có ưu điểm là hoạt động đơn giản, hiệu quả phục hồi nhanh và độ minh bạch cao hơn.',
  // join us
  joinText: 'Giao diện mẫu Web của sàn giao dịch',
  more: 'more',
  // Research Department
  researchTitle: 'Bộ nghiên cứu',
  researchContent: 'Với đội ngũ nghiên cứu mạnh mẽ và vững chắc. Cung cấp nhu cầu của từng đơn vị kinh doanh và cung cấp cho các nhà đầu tư thông tin thị trường nhanh chóng và chính xác. Thông qua quản lý cơ sở dữ liệu nghiên cứu, định kỳ xuất bản báo cáo các ngành, báo cáo chứng khoán, tạp chí hàng tuần và hàng quý. Xuất bản thông tin nghiên cứu chất lượng cao, đã tạo được uy tín trong ngành.',
  // Act of Entrustment
  entrustmentTitle: 'Ủy quyền toàn bộ',
  entrustmentContent: 'Kinh nghiệm đầu tư bình quân của đội ngũ quản lý và đầu tư tài sản AK là hơn năm năm, đã trải qua ít nhất hai chu kỳ thịnh vượng, đủ để nắm bắt các xung lực tài chính và cơ hội đầu tư. Điều đáng chú ý là chúng tôi đặc biệt coi trọng đến kiểm soát rủi ro và các cơ chế khác. Chúng tôi đã thiết lập một bộ cơ chế kiểm soát rủi ro, để kiểm soát rủi ro đầu tư, bảo vệ khách hàng đầu tư đối diện với cơ chế dừng lỗ và dừng lợi nhuận khi gặp rủi ro hệ thống thị trường.',
  // Finance Consulting
  financeTitle: 'Tư vấn tài chính',
  financeContent: 'Với đội ngũ nghiên cứu mạnh là trụ cột, phương pháp nghiên cứu nhấn mạnh phân tích cơ bản, bổ sung phân tích kỹ thuật và thực hành thị trường, để nắm bắt cơ hội lợi nhuận đầu tư và giảm thiểu rủi ro. Làm phong phú thêm tư vấn đầu tư định hướng cơ bản dài hạn. Tất cả khách hàng của công ty có thể đặt câu hỏi thông qua bộ phận kinh doanh, do nhân viên quản lý tài chính tóm tắt các dữ liệu của bộ phận nghiên cứu, trả lời và đưa ra lời khuyên đầu tư toàn diện.',
  // Why Choose Us
  chooseUsTitle: 'Lý do bạn nên chọn chúng tôi',
  professional: 'Chuyên nghiệp',
  reliable: 'Tin cậy',
  safe: 'An toàn',
  enjoyable: 'Vui vẻ',
  // Copyright
  Copyright: 'Copyright@2020 AK FINANCIAL | ALL right reserved'
}
