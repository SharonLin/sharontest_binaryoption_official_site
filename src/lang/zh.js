export default {
  devTitle: 'AK FINANCIAL',
  index: '首页',
  info: '消息',
  // menu
  home: '首页',
  about: '关于我们',
  join: '加入我们',
  // banner
  findOutMore: '了解更多',
  // about us
  aboutText: '随著人们经济水平的不断提高，投资理财活动也随之逐渐增多，大众参与的金融理财方式有基金、股票、加盟代理等多种，AK FINANCIAL作为其中之一，有著操作简单，回收效益快透明度也更高的优势。',
  // join us
  joinText: '交易平台的界面网页模板',
  more: 'more',
  // Research Department
  researchTitle: '研究部',
  researchContent: '研究员以坚强的研究阵容及踏实的研究，提供各业务单位之需求，并提供投资人迅速，确实的市场资讯。透过研究资料库的电脑化管理，定期出版产业报告、个股报告、周刊及季刊，高品质研究资讯的出版，已在业界树立良好声誉。',
  // Act of Entrustment
  entrustmentTitle: '全权委托',
  entrustmentContent: 'AK 资产管理与投资团队，平均投资经验达到五年以上，至少历经两个景气循环，足以掌握金融的脉动与投资机会。值得一题的是，我们对于风险控管等机制尤其重视，已建立一套控制投资风险的风险控管机制，以保障既有的投资客户在面临市场系统风险时的停损与停益机制。',
  // Finance Consulting
  financeTitle: '咨询理财',
  financeContent: '以坚强的研究团队为支柱，研究方法强调以基本分析为主，辅以技术分析及市场实务等，借以掌握投资获利机会并降低风险，研究员并作公司访谈，深入调查及掌握第一手资讯，借以丰富以长期基本面为导向的投资建议，凡本公司之客户均可透过经纪部门提出问题，由咨询理财人员汇总研究部门资料，予以回答，提供完善的投资建议。',
  // Why Choose Us
  chooseUsTitle: '為什麼選擇我們',
  professional: '專業的',
  reliable: '可靠',
  safe: '安全',
  enjoyable: '愉快的',
  // Copyright
  Copyright: 'Copyright@2020 AK FINANCIAL | ALL right reserved'
}
