export default {
  devTitle: 'AK FINANCIAL',
  index: 'Index',
  info: 'Info',
  // menu
  home: 'Inicio',
  about: 'Sobre nosotros',
  join: 'Únete a nosotros',
  // banner
  findOutMore: 'Conocer más',
  // about us
  aboutText: 'Con la mejora continua de la comprensión y el conocimiento económico de las personas, las actividades de inversión y gestión financiera han ido aumentando gradualmente. Los métodos de gestión financiera en los que el público participa se incluyen los fondos the inversion, acciones, agentes de franquicia y muchos más. Las opciones binarias son una de ellas y con una operación simple significa una recuperación rápida, eficiente y transparente que permite una mayor ventaja en este campo económico.',
  // join us
  joinText: 'Plantilla web de interfaz para plataforma comercial',
  more: 'more',
  // Research Department
  researchTitle: 'Departamento de investigación',
  researchContent: 'Con un equipo de investigación de alta calidad y un análisis sólido, nosotros, como investigadores, satisfacemos las necesidades de varias unidades de negocios y brindamos a los inversores información de mercado rápida y precisa. A través de la gestión informatizada de la base de datos de investigación, proporcionamos publicaciones periódicas de informes de la industria, informes del mercado de valores individuales, revistas semanales y trimestrales, y la publicación de información de investigación de alta calidad que ha establecido una buena reputación en la industria.',
  // Act of Entrustment
  entrustmentTitle: 'Acto de encomienda',
  entrustmentContent: 'La experiencia promedio de inversión del equipo de gestión de activos e inversión de AK es de más de cinco años, y pasará por al menos dos ciclos prósperos, suficientes para comprender el pulso financiero y las oportunidades de inversión. Vale la pena mencionar que prestamos especial atención al control de riesgos. Hemos establecido un conjunto de mecanismos de control de riesgos para controlar los riesgos de inversión para proteger las inversiones de nuestros clientes utilizando mecanismos de stop-loss y stop-profit cuando se enfrentan a riesgos de mercado.',
  // Finance Consulting
  financeTitle: 'Consultoría financiera',
  financeContent: 'Con un fuerte equipo de investigación como pilar, enfatizamos el análisis básico, complementado con análisis técnico y prácticas de mercado, para aprovechar las oportunidades de ganancias de inversión y a la misma vez reducir los riesgos. Los investigadores también llevan a cabo entrevistas con la empresa, investigaciones en profundidad y obtienen información de primera mano para enriquecer el asesoramiento de inversión orientado a largo plazo. Todos los clientes de la empresa pueden hacer preguntas a través del departamento de finanza, y el personal de gestión financiera consultará y resumirá la información de los departamentos de investigación para responder lo mejor posible y proporcionar asesoramiento integral sobre inversiones.',
  // Why Choose Us
  chooseUsTitle: '¿POR QUÉ ELEGIRNOS?',
  professional: 'Profesional',
  reliable: 'Confiable',
  safe: 'Seguro',
  enjoyable: 'Enjoyable',
  // Copyright
  Copyright: 'Copyright@2020 AK FINANCIAL | ALL right reserved'
}
