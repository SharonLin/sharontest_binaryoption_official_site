export default {
  devTitle: 'AK FINANCIAL',
  index: 'Index',
  info: 'Info',
  // menu
  home: 'Home',
  about: 'About us',
  join: 'Join us',
  // banner
  findOutMore: 'Find out more',
  // about us
  aboutText: 'With the continuous improvement of people’s understanding and economic knowledge. Investment and financial management activities have gradually been on the rise. The financial management methods that the public participates in include funds, stocks, franchise agents and many more. Binary options are one of them and with a simple operation means fast recovery, efficiency and transparency which allows for a higher advantage in this economic field.',
  // join us
  joinText: 'Interface web template for trading platform',
  more: 'more',
  // Research Department
  researchTitle: 'Research Department',
  researchContent: 'With a strong research team and solid analysis, we as researchers provide the needs of various business units and provide investors with rapid and accurate market information. Through the computerised management of the research database, we provide regular publication of industry reports, individual stock reports, weekly and quarterly journals, and the publication of high-quality research information that has established a good reputation in the industry.',
  // Act of Entrustment
  entrustmentTitle: 'Act of Entrustment',
  entrustmentContent: 'The average investment experience of the AK asset management and investment team is more than five years, and it will go through at least two prosperous cycles, enough to grasp the financial pulse and investment opportunities. It is worth mentioning that we pay special attention to risk control. We have established a set of risk control mechanisms to control investment risks to protect our clients’ investments using stop-loss and stop-profit mechanisms when facing market risks.',
  // Finance Consulting
  financeTitle: 'Finance Consulting',
  financeContent: 'With a strong research team as the pillar, we emphasise basic analysis, supplemented by technical analysis and market practice, so as to grasp investment profit opportunities and reduce risks. Researchers also conduct company interviews, in-depth investigations and grasp first-hand information in order to enrich long-term fundamental-oriented investment advice. All customers of the company can ask questions through the brokerage department, and the financial management staff will consults and summarise the research departments information in order to answer sufficiently and provide comprehensive investment advice.',
  // Why Choose Us
  chooseUsTitle: 'Why Choose Us',
  professional: 'Professional',
  reliable: 'Reliable',
  safe: 'Safe',
  enjoyable: 'Enjoyable',
  // Copyright
  Copyright: 'Copyright@2020 AK FINANCIAL | ALL right reserved'
}
