// import Login from '@/pc/views/Login'
import Main from '@/pc/views/Main'
import Index from '@/pc/views/Index'
import Info from '@/pc/views/Info'

// -----------------------------------------mobile區開始
// import LoginMobile from '@/mobile/views/Login'
import MainMobile from '@/mobile/views/Main'
import IndexMobile from '@/mobile/views/Index'
import InfoMobile from '@/mobile/views/Info'

export default ({
  defaultRoutes: [
    {
      path: '/pc/',
      name: 'Main',
      component: Main,
      children: [
        { path: '/index', name: 'index', component: Index },
        { path: '/info', name: 'info', component: Info }
      ]
    }
  ],
  defaultRoutesMobile: [
    {
      path: '/mobile/',
      name: 'Main',
      component: MainMobile,
      children: [
        { path: '/mobile/index', name: 'index', component: IndexMobile },
        { path: '/mobile/info', name: 'info', component: InfoMobile }
      ]
    }
  ]
})
