import yuanbaoLogo from '../assets/platformConfig/logo_yb.png'
import yuanbaoLogoMobile from '../assets/platformConfig/yuanbao-logo-mobile.png'

let platformConfig = {}
const beforeOnlineConfig = {
  title: 'beforeOnline'
}
const devConfig = {
  logo: yuanbaoLogo,
  logoMobile: yuanbaoLogoMobile,
  appId: 'dev-id',
  locale: {
    cn: 'ZH',
    // tw: 'TW',
    en: 'EN',
    es: 'ES',
    vi: 'VN',
    th: 'TH'
  },
  title: 'devTitle'
}
switch (process.env.VUE_APP_TITLE) {
  case 'beforeOnline':
    platformConfig = beforeOnlineConfig
    break
  default:
    platformConfig = devConfig
}
export default platformConfig
