const isProduction = process.env.NODE_ENV !== 'development'

const config = {
  options: {
    isEnabled: true,
    logLevel: isProduction ? 'error' : 'debug',
    stringifyArguments: false,
    showLogLevel: true,
    showMethodName: true,
    separator: '|',
    showConsoleColors: true
  },
  baseURL: '',
  routerBase: ''
}

export { config }
