const path = require('path')

const iconList = {
  beforeOnline: 'public/favicon_multi_chain.ico'
}
const icon = process.env.VUE_APP_TITLE ? iconList[process.env.VUE_APP_TITLE] : 'public/favicon_dev.ico'
const publicPath = process.env.NODE_ENV === 'production' ? './' : '/'

module.exports = { 
  publicPath,
  chainWebpack: config => {
    config.plugin('html').tap(
      args => {
        args[0].favicon = path.resolve(icon)    
        return args /* 传递给 html-webpack-plugin's 构造函数的新参数 */
      }
    )
  }
}
